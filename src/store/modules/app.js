import cityService from '../../api/cityService'
import provinceService from '../../api/provinceService'
import priceService from '../../api/priceService'
import realtyTypeService from '../../api/realtyTypeService'
import orderService from '../../api/orderService'
import userService from '../../api/userService'
import insuranceService from '../../api/insuranceService'
import contentService from '../../api/contentService'
import subscriptionService from '../../api/subscriptionService'
// import bgCompare from '../../assets/js/bgCompare'

import * as types from '../mutation-types'

import Vue from 'vue'

const state = {
  editedOrder: {},
  cities: [],
  currentCityId: null,
  provinces: [],
  realtyTypes: [],
  meters: null,
  flatMeters: null,
  officeMeters: null,
  hours: null,
  flatHours: null,
  officeHours: null,
  price: null,
  flatPrice: null,
  officePrice: null,
  realtyTypeId: null,
  allRealtyTypesPriceHoursMeters: [],
  step: 1,
  orderViewModel: null,
  user: null,
  checkedAddSubServicesObj: {},
  checkedAddSubServices: [],
  checkedAddSubServiceIds: [],
  checkedSubConditionIds: [],
  checkedSurfaceIds: [],
  checkedInsurances: [],
  insurances: [],
  checkedInsuranceIds: [],
  orderDate: null,
  orderTime: null,
  cleaners: [],
  cleanerId: null,
  buyClearKit: false,
  clearKit: null,
  comment: null,
  orders: [],
  content: [],
  registerErrors: {},
  loginErrors: null,
  subscriptions: [],
  order: {
    services: {}
  },
  myOrder: {
    services: {}
  },
  checkedAddServices: {},
  isExpress: null,
  firstImage: '',
  secondImage: '',
  subscription: null,
  invoiceNumber: '',
  subserviceHoursMeters: [],
  secretPlaceId: null,
  buySubscription: false,
  filter: {},
  isCouponActive: false,
  couponPercent: null,
  gestToken: null,
  secretPlaces: null,
  cleaner: null,
  maxStep: null,
  fixer: 1,
  article: null,
  choosedCategory: null,
  bathroom: null,
  selected_cleaner:{},
  frequency:null,
  direct_payment:null,
  clean_type_name:'',
  order_copy:null
}

// getters

const getters = {
  getMaxStep: (state) => state.maxStep,
  getCleanTypeName:(state)=>state.clean_type_name,
  getEditedOrder: (state) => state.editedOrder,
  getGestToken: (state) => {
    return state.gestToken
  },
  getBathroom:(state) =>state.bathroom,
  getOrderCopy:(state) =>state.order_copy,
  getSelectedCleaner:(state) =>state.selected_cleaner,
  getCouponPercent: (state) => {
    if (state.couponPercent !== null) {
      return state.couponPercent
    }
    return 1
  },
  getIsCouponActive: state => state.isCouponActive,
  getFilter: state => state.filter,
  getInvoiceNumber: state => state.invoiceNumber,
  getLoginErrors: state => state.loginErrors,
  getSubscription: state => state.subscription,
  getFlatPrice: state => state.flatPrice,
  getOfficePrice: state => state.officePrice,
  getFlatMeters: state => state.flatMeters,
  getOfficeMeters: state => state.officeMeters,
  getFlatHours: state => state.flatHours,
  getOfficeHours: state => state.officeHours,
  getFirstImage: state => state.firstImage,
  getSecondImage: state => state.secondImage,
  getDirectPayment:state => state.direct_payment,
  getIsExpress: () => {
    let today = new Date()
    let orderDate = new Date(state.orderDate)
    let timeDiff = Math.abs(today.getTime() - orderDate.getTime())
    let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))
    if (diffDays >= 2) {
      return null
    } else {
      localStorage.setItem('express', true)
      return 'express'
    }
  },
  getCheckedAddServices: state => {
    let res = {}
    for (let addServiceId in state.checkedAddServices) {
      res[addServiceId] = true
    }
    return res
  },
  order: state => state.order,
  myOrder: state => state.myOrder,
  allCities: state => state.cities,
  getCurrentCityId: state => {
    localStorage.setItem('cityId', !state.currentCityId ? state.cities[0].id : null)
    return !state.currentCityId ? state.cities[0].id : null
  },
  allProvinces: state => state.provinces,
  getMeters: state => {
      localStorage.setItem('flatMeters', state.flatMeters)
      return state.flatMeters
  },
  getFullHours: state => {
    let hours = state.hours
    // if (state.subscription && state.subscription.addServices) {
    //   for (let service in state.subscription.addServices) {
    //     if (service) {
    //       hours += 1
    //     }
    //   }
    // }
    for (let serviceId in state.checkedAddSubServicesObj) {
      hours += state.checkedAddSubServicesObj[serviceId].hours || 0
    }
    return hours
  },
  getHours: state => {
    localStorage.setItem('hours', state.hours)
    return state.hours
  },
  getPriceWithooutServ: state => {
    return state.price;
  },
  getPrice: state => {
    let subscription = state.subscription
    let totalPrice = 0

    // if (state.user && state.user.subscription_id && subscription) {
    //   let subscriptionPrice = state.subscription.price
    //
    //   totalPrice = state.hours * subscriptionPrice
    //   // console.log('user sub price:' + totalPrice)
    // }
    if (subscription !== null && state.buySubscription) {
      totalPrice = state.hours * state.subscription.price
      // console.log('buysub:' + totalPrice)
    }

    // if (subscription && state.buyClearKit) {
    //   totalPrice -= parseInt(subscription.buy_clear_kit * 5)
    // }
    // console.log('clear sub:' + totalPrice, state.user)
    if (totalPrice === 0) {
      totalPrice = totalPrice + state.price
      // console.log('def:' + totalPrice)
    }

    for (let serviceId in state.checkedAddSubServicesObj) {
      if (state.checkedAddSubServicesObj[serviceId].value) {
        totalPrice += state.checkedAddSubServicesObj[serviceId].price
      }
    }
    // console.log('serv:' + totalPrice)
    for (let insur in state.checkedInsurances) {
      totalPrice += parseInt(state.checkedInsurances[insur]['price'])
    }
    // console.log('insur:' + totalPrice)
    if (state.couponPercent !== null) {
      totalPrice *= state.couponPercent
    }
    // console.log('coupon:' + totalPrice)
    totalPrice += parseInt(state.buyClearKit * 5)
    // console.log('clear_kit:' + totalPrice)
    if (subscription !== null && state.buySubscription) {
      return Math.round((totalPrice * 4) * 100) / 100
    }

    return Math.round(totalPrice * 100) / 100
  },
  getStep: state => state.step,
  getRealtyTypeId: state => state.realtyTypeId,
  getArticle: state => state.article,
  getChoosedCategory: state => state.choosedCategory,
  getRealtyTypes: state => state.realtyTypes,
  getOrderViewModel: state => state.orderViewModel,
  getUser: state => state.user,
  getCheckedAddSubServices: state => state.checkedAddSubServices,
  getCheckedAddSubServiceIds: (state) => {
    let res = []
    for (let serviceId in state.checkedAddSubServicesObj) {
      res.push(parseInt(serviceId))
    }
    return res
  },
  getServicesFromPaymentStep: (state) => {
    let res = []
    for (let serviceId in state.checkedAddSubServicesObj) {
      if(Array.isArray(state.checkedAddSubServicesObj[serviceId].answer_id)){
          for(let subserviceId in state.checkedAddSubServicesObj[serviceId].answer_id){
            res.push(parseInt(state.checkedAddSubServicesObj[serviceId].answer_id[subserviceId]))
          }
      }
      else{
        if(state.checkedAddSubServicesObj[serviceId].answer_id){
          res.push(parseInt(state.checkedAddSubServicesObj[serviceId].answer_id))
        }
        else{
          res.push(parseInt(state.checkedAddSubServicesObj[serviceId].id))
        }
      }
    }
    return res
  },
  getCheckedAddSubServicesObj: state => state.checkedAddSubServicesObj,
  getCheckedSubConditionIds: state => state.checkedSubConditionIds,
  getCheckedSurfaceIds: state => state.checkedSurfaceIds,
  getInsurances: state => state.insurances,
  getInsuranceIds: state => state.checkedInsuranceIds,
  getCheckedInsurances: state => state.checkedInsurances,
  getOrderDate: state => state.orderDate,
  getOrderTime: state => state.orderTime,
  getCleaners: state => state.cleaners,
  getCleanerId: state => state.cleanerId,
  getBuyClearKit: state => state.buyClearKit,
  getClearKit: state => state.clearKit,
  getAllRealtyTypesPriceHoursMeters: state => {
    let allMeters = state.allRealtyTypesPriceHoursMeters
    let houseMeters = allMeters.filter(item => item.realty_type === 'Casa')
    let officeMeters = allMeters.filter(item => item.realty_type === 'Business')

    return {
      houseMeters,
      officeMeters
    }
  },
  getComment: state => state.comment,
  getOrders: state => state.orders,
  getContent: state => state.content,
  getRegisterErrors: state => state.registerErrors,
  getSubscriptions: state => state.subscriptions,
  getCleanerOrders: (state) => {
    let cleanerOrders = state.orders.filter(order => order.cleaner !== null)
    return cleanerOrders.sort((curOrder, prevOrder) => {
      let curDate = new Date(curOrder.date.date)
      let prevDate = new Date(prevOrder.date.date)
      return curDate < prevDate
    })
  },
  getSubserviceHoursMeters: state => state.subserviceHoursMeters,
  getSecretPlaceId: state => state.secretPlaceId,
  getBuySubscription: state => state.buySubscription,
  getSecretPlaces: state => state.secretPlaces,
  getCleaner: state => state.cleaner,
  getFrequency: state => state.frequency
}

// actions

const actions = {
  updateGetterCache ({state}) {
    state.fixer++
    state.fixer--
  },
  setDirectPayment({commit},data){
    commit(types.SET_DIRECT_PAYMENT,data)
  },
  setCleanTypeName({commit},data){
    commit(types.SET_CLEAN_TYPE_NAME,data)
  },
  setFrequency ({commit}, data) {
    commit(types.SET_FREQUENCY,data)
    //state.frequency = data
  },
  setMaxStep ({state}, data) {
    state.maxStep = data
  },
  setSelectedCleaner({commit}, data){
    commit(types.SET_SELECTED_CLEANER,data)
  },
  setBathroom({commit}, data){
    commit(types.SET_BATHROOM,data)
  },
  setSecretPlaces ({state}, data) {
    state.secretPlaces = data
  },
  setCleaner ({state}, data) {
    state.cleaner = data
  },
  setEditedOrder ({state}, data) {
    state.editedOrder = data
  },
  setGestToken (token) {
    state.gestToken = token
  },
  setCouponPercent ({commit}, data) {
    commit(types.SET_COUPON_PERCENT, data)
  },
  setIsCouponActive ({commit}, data) {
    commit(types.SET_IS_COUPON_ACTIVE, data)
  },
  setExpress ({commit}, data) {
    commit(types.SET_EXPRESS, data)
  },
  setInvoiceNumber ({commit}, data) {
    commit(types.SET_INVOICE_NUMBER, data)
  },
  setRegisterErrors ({commit}, data) {
    commit(types.SET_REGISTER_ERRORS, data)
  },
  setLoginErrors ({commit}, data) {
    commit(types.SET_LOGIN_ERROR, data)
  },
  setArticle ({commit}, data) {
    commit(types.SET_ARTICLE, data)
  },
  setChoosedCategory ({commit}, data) {
    commit(types.SET_CATEGORY, data)
  },
  setSubscription ({commit}, data) {
    commit(types.SET_SUBSCRIPTION, data)
  },
  setFlatHours ({commit}, data) {
    commit(types.SET_FLAT_HOURS, data)
  },
  setOfficeHours ({commit}, data) {
    commit(types.SET_OFFICE_HOURS, data)
  },
  setFlatPrice ({commit}, data) {
    commit(types.SET_FLAT_PRICE, data)
  },
  setOfficePrice ({commit}, data) {
    commit(types.SET_OFFICE_PRICE, data)
  },
  setFlatMeters ({commit}, data) {
    commit(types.SET_FLAT_METERS, data)
  },
  setOfficeMeters ({commit}, data) {
    commit(types.SET_OFFICE_METERS, data)
  },
  setImgSlider ({commit}, data) {
    commit(types.SET_IMG_SLIDER, data)
  },
  getCities ({commit}, callback) {
    cityService.getAll().then(response => {
      commit(types.SET_CITIES, response.data.data)
      if (typeof callback === 'function') {
        callback()
      }
    })
  },
  setCurrentCityId ({commit}, data) {
    commit(types.SET_CURRENT_CITY_ID, data)
  },
  getProvinces ({commit}, callback) {
    provinceService.getAll().then(response => {
      commit(types.SET_PROVINCES, response.data.data)
      callback()
    })
  },
  setHours ({commit}, hours) {
    commit(types.SET_HOURS, hours)
  },
  setPriceFromMeters ({commit, state}, data) {
    priceService.calculatePriceFromMeters(data.meters, data.realtyTypeId).then(response => {
      let subscription = state.subscription
      if (subscription !== null) {
       // if (state.realtyTypeId === 1) {
         commit(types.SET_FLAT_PRICE, response.data.data.hours * state.subscription.price)
         commit(types.SET_FLAT_HOURS, response.data.data.hours)
      //  }
       // if (state.realtyTypeId === 2) {
         // commit(types.SET_PRICE, response.data.data.hours * state.subscription.price)
         // commit(types.SET_OFFICE_HOURS, response.data.data.hours)
      } else {
          commit(types.SET_FLAT_PRICE, response.data.data.price)
          commit(types.SET_FLAT_HOURS, response.data.data.hours)
      }
    })
      .catch(err => {
        commit(types.SET_PRICE, null)
        commit(types.SET_HOURS, null)
        console.log(err.message)
      })
  },
  setPriceFromHours ({commit, state}, data) {
    priceService.calculatePriceFromHours(data.hours, data.realtyTypeId, data.date, data.time).then(response => {
      let subscription = state.subscription
      if (subscription !== null) {
        //if (state.realtyTypeId === 1) {
          commit(types.SET_FLAT_PRICE, data.hours * state.subscription.price)
          commit(types.SET_FLAT_METERS, response.data.data.meters)
        //}
        // if (state.realtyTypeId === 2) {
        //   commit(types.SET_OFFICE_PRICE, data.hours * state.subscription.price)
        //   commit(types.SET_OFFICE_METERS, response.data.data.meters)
        // }
      } else {
        //if (state.realtyTypeId === 1) {
          commit(types.SET_FLAT_PRICE, response.data.data.price)
          commit(types.SET_FLAT_METERS, response.data.data.meters)
       // }
        // if (state.realtyTypeId === 2) {
        //   commit(types.SET_OFFICE_PRICE, response.data.data.price)
        //   commit(types.SET_OFFICE_METERS, response.data.data.meters)
        // }
      }
    })
      .catch(err => {
        commit(types.SET_PRICE, null)
        commit(types.SET_METERS, null)
        console.log(err.message)
      })
  },
  resetOrder ({commit}) {
    commit('resetOrder')
  },
  setImagesData ({commit}, data) {
    commit('setImagesData', data)
  },
  changePricePlus ({commit, store}, price) {
    commit(types.CHANGE_PRICE_PLUS, price)
  },
  changePriceMinus ({commit}, price) {
    commit(types.CHANGE_PRICE_MINUS, price)
  },
  setStep ({commit}, step) {
    commit(types.SET_STEP, step)
  },
  getRealtyTypes ({commit}, step) {
    realtyTypeService.getRealtyTypes().then(response => {
      commit(types.GET_REALTY_TYPES, response.data.data)
    }).catch(err => {
      console.log(err.message)
    })
  },
  setRealtyTypeId ({commit}, realtyTypeId) {
    commit(types.SET_REALTY_TYPE_ID, realtyTypeId)
  },
  getOrderViewModel ({commit}, callback) {
    orderService.getOrderViewModel().then(response => {
      commit(types.GET_ORDER_VIEW_MODEL, response.data)
      callback()
    })
  },
  setUser ({commit, state}, data) {
    state.user = data
  },
  setUserWithCard ({commit, state}, data) {
    state.user = data.user
    commit(types.ADD_CARD, data.card)
  },
  logOutUser ({commit}, success) {
    commit(types.LOGOUT_USER)
    success()
  },
  getCurrentUser ({dispatch, commit}) {
    return new Promise((resolve, reject) => {
      userService.getCurrentUser().then(response => {
        let dates = {availableDates: null}
        dates.availableDates = response.data.meta.availableDates.sort(function (first, second) {
          return first.date > second.date
        })
        commit(types.GET_CURRENT_USER, response.data.data)
        if (response.data.meta.card) {
          commit(types.ADD_CARD, response.data.meta)
        }
        commit(types.SET_AVAILABLE_DATES, dates)
        // dispatch('getSubscriptions').then(() => {
        //   commit(types.SET_AVAILABLE_DATES, dates)
        //   resolve()
        // })
        resolve()
      }).catch(err => {
        console.log(err)
        reject(err)
      })
    })
  },
  login ({commit, dispatch}, data) {
    userService.login(data.user.email, data.user.password).then(response => {
      Vue.auth.setToken(response.data.access_token, response.data.expires_in + Date.now())
      dispatch('getCurrentUser').then(() => {
        console.log('resolve')
        data.resolve()
      }
      )
    }).catch(() => {
      commit(types.SET_LOGIN_ERROR, 'Login non eseguito, riprova utilizzando altri dati')
      data.reject()
    })
  },
  register ({commit}, data) {
    userService.register(data.user).then(response => {
      commit(types.GET_CURRENT_USER, response.data)
      userService.login(data.user.email, data.user.password).then(res => {
        Vue.auth.setToken(res.data.access_token, res.data.expires_in + Date.now())
        data.success()
        commit(types.GET_CURRENT_USER, data.user)
      })
    }).catch(err => {
      let status = err.message.split(' ')[5]
      if (+status === 422) {
        commit(types.SET_REGISTER_ERRORS, err.response.data)
      }
    })
  },
  setCheckedSubConditionIds ({commit}, data) {
    commit(types.SET_CHECKED_SUB_CONDITIONS_IDS, data.id)
  },
  removeCheckedSubConditionIds ({commit, state}, data) {
    // state.orderViewModel.conditions.forEach((condition) => {
    //   condition.subConditions.forEach((subCondition) => {
    //     if (subCondition.id === data.id) {
    //       let index = condition.subConditions.indexOf(subCondition.id)
    //       commit(types.REMOVE_CHECKED_SUB_CONDITIONS_IDS, index)
    //     }
    //   })
    // })
    let index = state.checkedSubConditionIds.indexOf(data.id)
    commit(types.REMOVE_CHECKED_SUB_CONDITIONS_IDS, index)
  },
  setCheckedSurfaceIds ({commit}, data) {
    commit(types.SET_CHECKED_SURFACE, data)
  },
  removeCheckedSurfaceIds ({commit, state}, data) {
    state.orderViewModel.surfaces.forEach((surface) => {
      if (surface.id === data.id) {
        let index = state.orderViewModel.surfaces.indexOf(surface.id)
        commit(types.REMOVE_CHECKED_SURFACES, index)
      }
    })
  },
  getInsurances ({commit}) {
    insuranceService.getAll().then(response => {
      commit(types.GET_INSURANCES, response.data.data)
    })
  },
  setInsurances ({commit, state}, data) {
    state.orderViewModel.insurances.forEach((insurance) => {
      if (insurance.id === data.id) {
        commit(types.SET_CHECKED_INSURANCES, insurance)
      }
    })
  },
  removeInsurances ({commit, state}, data) {
    state.orderViewModel.insurances.forEach((insurance) => {
      if (insurance.id === data.id) {
        commit(types.REMOVE_CHECKED_INSURANCES, insurance.id)
      }
    })
  },
  addInsuranceIds ({commit}, data) {
    commit(types.SET_INSURANCE_IDS, data)
  },
  removeInsuranceId ({commit}, data) {
    commit(types.REMOVE_INSURANCE_IDS, data)
  },
  pushAddSubServiceById ({commit, state}, data) {
    state.orderViewModel.addServices.forEach((addService) => {
      addService.addSubServices.forEach((addSubService) => {
        if (addSubService.id === data.id) {
          commit(types.SET_CHECKED_ADD_SUB_SERVICES_IDS, addSubService.id)
          commit(types.SET_CHECKED_ADD_SUB_SERVICES, addSubService)
        }
      })
    })
  },
  removeAddSubServiceById ({commit, state}, data) {
    state.orderViewModel.addServices.forEach((addService) => {
      addService.addSubServices.forEach((addSubService) => {
        if (addSubService.id === data.id) {
          let index = state.checkedAddSubServices.indexOf(addSubService)
          commit(types.REMOVE_CHECKED_ADD_SUB_SERVICES_IDS, index)
          commit(types.REMOVE_CHECKED_ADD_SUB_SERVICES, index)
        }
      })
    })
  },
  setOrderDate ({commit}, data) {
    commit(types.SET_ORDER_DATE, data)
  },
  setOrderCopy({commit}, data){
    commit(types.SET_ORDER_COPY, data)
  },
  setOrderTime ({commit}, data) {
    commit(types.SET_ORDER_TIME, data)
  },
  setCleaners ({commit}, data) {
    userService.getCleaners(data.data).then(response => {
      commit(types.SET_CLEANERS, response.data.data)
      data.success()
    })
  },
  setCleanerId ({commit}, data) {
    commit(types.SET_CLEANER_ID, data)
  },
  setBuyClearKit ({commit}, data) {
    commit(types.SET_BUY_CLEAR_KIT, data)
  },
  setAllRealtyTypesPriceHoursMeters ({commit}) {
    realtyTypeService.getAllRealtyTypesPriceHoursMeters().then(response => {
      commit(types.GET_ALL_REALTY_TYPES_PRICE_HOURS_METERS, response.data.data)
    })
  },
  setComment ({commit}, data) {
    commit(types.SET_COMMENT, data)
  },
  setOrders ({commit, state}, data) {
    state.orders = data
  },
  setFormOrder ({commit, state}, data) {
    let order = JSON.parse(JSON.stringify(data))
    order.add_services = order.add_services.map((service) => service.id)
    order.add_surfaces = order.add_surfaces.map((service) => service.id)
    order.sub_conditions = order.sub_conditions.map((service) => service.id)
    order.insurances = order.insurances.map((service) => service.id)
    state.order = order
  },
  getOrders ({commit}) {
    orderService.getOrders().then(response => {
      commit(types.GET_ORDERS, response.data.data)
    })
  },
  getContent ({commit}) {
    contentService.getContent().then(response => {
      commit(types.GET_CONTENT, response.data.data)
    })
  },
  removeClearKit ({commit}) {
    commit(types.REMOVE_CLEAR_KIT)
  },
  getSubscriptions ({dispatch, commit, state}) {
    return new Promise((resolve, reject) => {
      subscriptionService.getAll().then(response => {
        commit(types.SET_SUBSCRIPTIONS, response.data.data)
        dispatch('setAddSubServicesBySubscription', state.subscription).then(() => {
          if (state.subscription && state.subscription.buy_clear_kit) {
            commit(types.SET_BUY_CLEAR_KIT, state.subscription.buy_clear_kit)
          }
          resolve()
        })
      }).catch(err => {
        console.log(err)
        reject(err)
      })
    })
  },
  setPrice ({commit}, data) {
    commit(types.SET_PRICE, data)
  },
  setBuyClearKitBySubscription ({commit}, data) {
    if (data.buy_clear_kit) {
      commit(types.SET_BUY_CLEAR_KIT, data.buy_clear_kit)
    }
  },
  setAddSubServicesBySubscription ({commit}, data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let subscription = state.subscriptions.find((item) => {
          return item.id === data.id
        })
        let choosenAddServiceIds = state.checkedAddSubServicesObj
        for (let addServiceId in subscription.addServices) {
          choosenAddServiceIds[subscription.addServices[addServiceId].id] = {
            title: subscription.addServices[addServiceId].title,
            price: subscription.addServices[addServiceId].price,
            value: false,
            realty_type_id: subscription.addServices[addServiceId].realty_type_id
          }
        }
        localStorage.setItem('subscription', JSON.stringify({
          'title': subscription.title,
          'addServices': choosenAddServiceIds
        }))
        commit(types.SET_CHECHEKED_ADD_SUB_SERVICES_OBJ, choosenAddServiceIds)
      }
      resolve()
    })
  },
  setCheckedAddSubServicesObj ({commit}, data) {
    commit(types.SET_CHECHEKED_ADD_SUB_SERVICES_OBJ, data)
  },
  setCheckedAddSubServices ({state}, data) {
    state.checkedAddSubServices = data
  },
  changeCheckedServices ({commit}, data) {
    if (!data.isRemove) {
      commit(types.SET_ADD_SERVICES_OBJ, {
        id: data.id,
        value: data.value,
        hours: data.hours,
        title: data.title,
        price: data.full_price,
        realty_type_id: data.realty_type_id,
        answer_id:data.answer || null
      })
    }
    if (data.isRemove) {
      commit(types.REMOVE_ADD_SERVICES_OBJ, {
        id: data.id
      })
    }
  },
  getOrderJSON ({commit, state}) {
    let data = JSON.stringify(state.order)
    return JSON.parse(data)
  },
  setSubserviceHoursMeters ({state}, data) {
    state.subserviceHoursMeters = data.data
  },
  setSecretPlaceId ({state}, data) {
    state.secretPlaceId = data
  },
  setBuySubscription ({state}, data) {
    state.buySubscription = data
  },
  setOrder ({commit}, data) {
    commit('setOrder', data)
  }
}

// mutations

const mutations = {
  [types.SET_ORDER_COPY] (state, data) {
    state.order_copy = data
  },
  [types.SET_BATHROOM] (state, data) {
    state.bathroom = data
  },
  [types.SET_DIRECT_PAYMENT] (state, data) {
    state.direct_payment = data
  },
  [types.SET_SELECTED_CLEANER] (state, data){
    state.selected_cleaner = data
  },
  [types.SET_FREQUENCY] (state, data){
    state.frequency = data
  },
  [types.SET_CLEAN_TYPE_NAME] (state, data){
    state.clean_type_name = data
  },
  [types.SET_COUPON_PERCENT] (state, data) {
    state.couponPercent = data
  },
  [types.SET_IS_COUPON_ACTIVE] (state, data) {
    state.isCouponActive = data
  },
  [types.SET_EXPRESS] (state, data) {
    state.isExpress = data
  },
  [types.SET_INVOICE_NUMBER] (state, data) {
    state.invoiceNumber = data + state.user.id.toString()
  },
  [types.SET_LOGIN_ERROR] (state, data) {
    state.loginErrors = data
  },
  [types.LOGOUT_USER] (state) {
    state.user = null
    state.subscription = null
    state.checkedAddServices = null
    state.checkedAddSubServiceIds = null
    state.checkedAddSubServicesObj = {}
    state.clearKit = null
  },
  [types.SET_SUBSCRIPTION] (state, data) {
    state.subscription = data
  },
  [types.SET_FLAT_HOURS] (state, data) {
    state.flatHours = data
  },
  [types.SET_OFFICE_HOURS] (state, data) {
    state.officeHours = data
  },
  [types.SET_FLAT_METERS] (state, data) {
    let subscription = state.subscription
    if (subscription !== null) {
      state.flatPrice = state.subscription.price * state.flatHours
    }
    state.flatMeters = data
  },
  [types.SET_OFFICE_METERS] (state, data) {
    let subscription = state.subscription
    if (subscription !== null) {
      state.officePrice = state.subscription.price * state.officeHours
    }
    state.officeMeters = data
  },
  [types.SET_FLAT_PRICE] (state, data) {
    state.price = data
  },
  [types.SET_OFFICE_PRICE] (state, data) {
    state.officePrice = data
  },
  [types.SET_IMG_SLIDER] (state, data) {
    state.imgSlider = data
  },
  [types.SET_ORDER_SERVICES] (state, data) {
    state.myOrder.services = data
  },
  [types.SET_CITIES] (state, cities) {
    state.cities = cities
  },
  [types.SET_CURRENT_CITY_ID] (state, data) {
    state.currentCityId = data
  },
  [types.SET_PROVINCES] (state, provinces) {
    state.provinces = provinces
  },
  [types.SET_METERS] (state, meters) {
    state.meters = meters
  },
  [types.SET_HOURS] (state, hours) {
    state.hours = hours
  },
  [types.SET_PRICE] (state, price) {
    state.price = +price
  },
  [types.CHANGE_PRICE_PLUS] (state, price) {
    state.price += +price
  },
  [types.CHANGE_PRICE_MINUS] (state, price) {
    state.price -= +price
  },
  [types.SET_STEP] (state, step) {
    state.step = step
  },
  [types.GET_REALTY_TYPES] (state, realtyTypes) {
    state.realtyTypes = realtyTypes
  },
  [types.SET_REALTY_TYPE_ID] (state, realtyTypeId) {
    state.realtyTypeId = realtyTypeId
  },
  [types.GET_ORDER_VIEW_MODEL] (state, data) {
    state.orderViewModel = data.data
  },
  [types.GET_CURRENT_USER] (state, data) {
    state.user = data
  },
  [types.ADD_CARD] (state, data) {
    state.user = Object.assign(state.user, data.card)
  },
  [types.SET_AVAILABLE_DATES] (state, data) {
    state.user.availableDates = data.availableDates
  },
  [types.SET_CHECKED_ADD_SUB_SERVICES_IDS] (state, data) {
    state.checkedAddSubServiceIds.push(data)
  },
  [types.SET_CHECKED_ADD_SUB_SERVICES] (state, data) {
    state.checkedAddSubServices.push(data)
  },
  [types.REMOVE_CHECKED_ADD_SUB_SERVICES_IDS] (state, data) {
    state.checkedAddSubServiceIds.splice(data, 1)
  },
  [types.REMOVE_CHECKED_ADD_SUB_SERVICES] (state, data) {
    state.checkedAddSubServices.splice(data, 1)
  },
  [types.SET_CHECKED_SURFACE] (state, data) {
    state.checkedSurfaceIds.push(data.id)
  },
  [types.REMOVE_CHECKED_SURFACES] (state, data) {
    state.checkedSurfaceIds.splice(data, 1)
  },
  [types.SET_CHECKED_SUB_CONDITIONS_IDS] (state, data) {
    state.checkedSubConditionIds.push(data)
  },
  [types.REMOVE_CHECKED_SUB_CONDITIONS_IDS] (state, data) {
    state.checkedSubConditionIds.splice(data,1)
  },
  [types.GET_INSURANCES] (state, data) {
    state.insurances = data
  },
  [types.SET_INSURANCE_IDS] (state, data) {
    state.checkedInsuranceIds.push(data)
  },
  [types.REMOVE_INSURANCE_IDS] (state, data) {
    let index = state.checkedInsuranceIds.indexOf(data)
    state.checkedInsuranceIds.splice(index, 1)
  },
  [types.SET_CHECKED_INSURANCES] (state, data) {
    state.checkedInsurances.push(data)
  },
  [types.REMOVE_CHECKED_INSURANCES] (state, data) {
    let index = state.checkedInsurances.indexOf(data)
    state.checkedInsurances.splice(index, 1)
  },
  [types.SET_ORDER_DATE] (state, data) {
    state.orderDate = data
  },
  [types.SET_ORDER_TIME] (state, data) {
    state.orderTime = data
  },
  [types.SET_CLEANERS] (state, data) {
    state.cleaners = data
  },
  [types.SET_CLEANER_ID] (state, data) {
    state.cleanerId = data
  },
  [types.SET_BUY_CLEAR_KIT] (state, data) {
    state.buyClearKit = data
    if (data) {
      let value = false
      if (state.subscription) {
        value = state.subscription.buy_clear_kit
      }
      state.clearKit = [
        {
          title: 'Kit detersivi',
          price: 5,
          value: value
        }
      ]
    } else {
      state.clearKit = []
    }
  },
  [types.REMOVE_CLEAR_KIT] (state) {
    state.clearKit = []
  },
  [types.GET_ALL_REALTY_TYPES_PRICE_HOURS_METERS] (state, data) {
    state.allRealtyTypesPriceHoursMeters = data
  },
  [types.SET_COMMENT] (state, data) {
    state.comment = data
  },
  [types.GET_ORDERS] (state, data) {
    state.orders = data
  },
  [types.GET_CONTENT] (state, data) {
    state.content = data
  },
  [types.SET_REGISTER_ERRORS] (state, data) {
    state.registerErrors = data
  },
  [types.SET_ARTICLE] (state, data) {
    state.article = data
  },
  [types.SET_CATEGORY] (state, data) {
    state.choosedCategory = data
  },
  [types.SET_SUBSCRIPTIONS] (state, data) {
    state.subscriptions = data
    if (state.user && state.user.subscription_id && !state.buySubscription) {
      let subscription = state.subscriptions.find(sub => sub.id === state.user.subscription_id)
      state.subscription = subscription
    }
  },
  [types.SET_CHECHEKED_ADD_SUB_SERVICES_OBJ] (state, data) {
    state.checkedAddSubServicesObj = data
    state.checkedAddServices = data
    localStorage.setItem('checkedAddSubServicesObj', JSON.stringify(state.checkedAddSubServicesObj))
  },
  [types.SET_ADD_SERVICES_OBJ] (state, data) {
    let tmp = state.checkedAddSubServicesObj
    state.checkedAddSubServicesObj = {}
    state.checkedAddSubServicesObj = tmp
    let addService = state.orderViewModel.addServices.find(item => {
      return item.id === data.id
    })
    state.checkedAddSubServicesObj[data.id] = {
      id: data.id,
      title: data.title ? data.title : addService.title,
      price: data.price,
      value: data.value,
      hours: data.hours,
      realty_type_id: data.realty_type_id,
      answer_id:data.answer_id || null
    }
    state.price++
    state.price--
    localStorage.setItem('checkedAddSubServicesObj', JSON.stringify(state.checkedAddSubServicesObj))
  },
  [types.REMOVE_ADD_SERVICES_OBJ] (state, data) {
    let tmp = state.checkedAddSubServicesObj
    state.checkedAddSubServicesObj = {}
    state.checkedAddSubServicesObj = tmp
    delete state.checkedAddSubServicesObj[data.id]
    localStorage.setItem('checkedAddSubServicesObj', JSON.stringify(state.checkedAddSubServicesObj))
    state.price++
    state.price--
  },
  resetOrder (state) {
    state.buyClearKit = false
    state.checkedAddSubServicesObj = {}
    state.checkedAddSubServices = []
    state.checkedAddSubServiceIds = []
    state.checkedSubConditionIds = []
    state.checkedSurfaceIds = []
    state.checkedInsurances = []
  },
  setImagesData (state, data) {
    state.firstImage = data.firstImage
    state.secondImage = data.secondImage
    // bgCompare({
    //   beforeImage: state.secondImage,
    //   afterImage: state.firstImage,
    //   bgSize: 'cover',
    //   targetId: 'demo',
    //   handleTheme: 'dark-theme',
    //   sliderPos: '5%'
    // })
  },
  setOrder (state, data) {
    state.order.comment = data
  },
  setPlaceId (state, data) {
    state.secretPlaceId = data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
