export default {
  getPriceFromAddSubServiceById (addServices, id) {
    let price = 0
    addServices.forEach((addService) => {
      addService.addSubServices.forEach((addSubService) => {
        if (addSubService.id === id) {
          price = addSubService.price
        }
      })
    })
    return price
  },
  getAddSubService (addServices, id) {
    let res = {}
    addServices.forEach((addService) => {
      addService.addSubServices.forEach((addSubService) => {
        if (addSubService.id === id) {
          res = addSubService
        }
      })
    })
    return res
  },
  getPriceFromInsuranceById (insurances, id) {
    let price = 0
    insurances.forEach((insurance) => {
      if (insurance.id === id) {
        price = insurance.price
      }
    })
    return price
  },
  finder (table, find, valueGetter) {
    let tableElement = table
      .sort(function (a, b) {
        return valueGetter(b) - valueGetter(a)
      })
      .reduce(function (previousValue, currentValue, index, array) {
        let diff = valueGetter(currentValue) - find

        if (previousValue.object) {
          if (diff >= 0 && previousValue.diff > diff) {
            return {
              diff: diff,
              object: currentValue
            }
          }

          return previousValue
        }

        return {
          diff: diff,
          object: currentValue
        }
      }, {
        diff: null,
        object: null
      })

    return tableElement.object.hours
  }
}
