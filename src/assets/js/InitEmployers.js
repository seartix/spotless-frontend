import $ from 'jquery'
import filterizr from './jquery.filterizr.min'
function init () {
  window.filterizr = filterizr
  $(document).ready(function () {
    // Set the carousel options
    $('#quote-carousel').carousel({
      pause: true
    })
  })
  $(document).on('ready', function () {
    $('.read-only-rating').rating({ displayOnly: true, step: 1 })
  })

}