import $ from 'jquery'

export default function () {
  $(function () {
    var headertext = []
    var headers = document.querySelectorAll('.booking-list-table th')
    var tablebody = document.querySelector('.booking-list-table tbody')

    for (let i = 0; i < headers.length; i++) {
      let current = headers[i]
      headertext.push(current.textContent.replace(/\r?\n|\r/, ''))
    }
    for (let i = 0, row = tablebody.rows[i]; ; i++) {
      for (let j = 0, col = row.cells[j]; ; j++) {
        col.setAttribute('data-th', headertext[j])
      }
    }
  }())
}
