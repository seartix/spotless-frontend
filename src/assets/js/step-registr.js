import $ from 'jquery'

export default function () {
  $(document).ready(function () {
    var navListItems = $('ul.setup-panel li a')
    let allWells = $('.setup-content')

    allWells.hide()

    navListItems.click(function (e) {
      e.preventDefault()
      var $target = $($(this).attr('href'))
      var $item = $(this).closest('li')

      if (!$item.hasClass('disabled')) {
        navListItems.closest('li').removeClass('active')
        $item.addClass('active')
        allWells.hide()
        $target.show()
      }
    })

    $('ul.setup-panel li.active a').trigger('click')
  })
}
