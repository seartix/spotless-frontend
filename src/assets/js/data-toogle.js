import $ from 'jquery'
export default function () {
  $(document).ready(function () {
    $('.toggle-form-btn').click(function () {
      $('.first-section-elem.tabs.tabs-custom').slideToggle()
    })
  })
}

