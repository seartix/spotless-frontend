import $ from 'jquery'

export default function () {
  $(function () {
    $('.header-nav .list a').click(function () {
      $('.header-nav .list a.active-menu').removeClass('active-menu')
      $(this).addClass('active-menu')
    })
    // $('.service-includes-elem-button button').click(function () {
    //   $(this).toggleClass('active-button')
    // })
    $('.service-materials-button').click(function () {
      $(this).toggleClass('service-materials-active-button')
    })

    // $('.profile-block-elem').click(function () {
    //   $(this).toggleClass('profile-block-elem-active')
    // })

    // HAMBURGER
    $('#nav-icon3').click(function () {
      $(this).toggleClass('open')
    })

    $('.services-additional-elem').hover(function () {
      $(this).find('.services-additional-elem-hover').show(400)
      $(this).find('.main-button').hide(400)
    },

    function () {
      $(this).find('.services-additional-elem-hover').hide(400)
      $(this).find('.main-button').show(400)
    })

    $('.service-difficult-elem ').hover(function () {
      $(this).find('.service-difficult-elem-hover').animate({
        opacity: 1,
        top: '+=35',
        height: 'toggle'
      }, 400, function () {
        // Animation complete.
      })
      $(this).css('z-index', '300')
    },
    function () {
      $(this).find('.service-difficult-elem-hover').animate({
        opacity: 0,
        top: '-=35',
        height: 'toggle'
      }, 400, function () {
        // Animation complete.
      })
      $(this).css('z-index', '0')
    })
    // SELECT-SEARCH

    // $('select').each(function () {
    //   let $this = $(this)
    //   let numberOfOptions = $(this).children('option').length
    //
    //   $this.addClass('select-hidden')
    //   $this.wrap('<div class="select"></div>')
    //   $this.after('<div class="select-styled"></div>')
    //
    //   var $styledSelect = $this.next('div.select-styled')
    //   $styledSelect.text($this.children('option').eq(0).text())
    //
    //   var $list = $('<ul />', {
    //     'class': 'select-options'
    //   }).insertAfter($styledSelect)
    //
    //   for (var i = 0; i < numberOfOptions; i++) {
    //     $('<li />', {
    //       text: $this.children('option').eq(i).text(),
    //       rel: $this.children('option').eq(i).val()
    //     }).appendTo($list)
    //   }
    //
    //   var $listItems = $list.children('li')
    //
    //   $styledSelect.click(function (e) {
    //     e.stopPropagation()
    //     $('div.select-styled.active').not(this).each(function () {
    //       $(this).removeClass('active').next('ul.select-options').hide()
    //     })
    //     $(this).toggleClass('active').next('ul.select-options').toggle()
    //   })
    //
    //   $listItems.click(function (e) {
    //     e.stopPropagation()
    //     $styledSelect.text($(this).text()).removeClass('active')
    //     $this.val($(this).attr('rel'))
    //     $list.hide()
    //   })
    //
    //   $(document).click(function () {
    //     $styledSelect.removeClass('active')
    //     $list.hide()
    //   })
    // })

    // STEP REGISTRATION

    $('.profile-block').owlCarousel({
      loop: false,
      nav: true,
      margin: 20,
      dots: false,
      autoPlay: false,
      responsive: {
        0: {
          items: 1
        },
        767: {
          items: 3
        },
        992: {
          items: 3
        }
      }
    })

    // MODAL WINDOW

    $('.look-profile').click(function () {
      $('#myModal').modal()
    })
  })
}
