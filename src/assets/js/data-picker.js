import $ from 'jquery'

export default function () {
  $(function () {
    $('.flatpickr').flatpickr({
      altInput: true,
      altFormat: 'j F,Y',

    })

    // $('.flatpickr-calendar').flatpickr({
    //   altInput: true,
    //   altFormat: 'j F,Y'
    //
    // })
    // $('.flatpickr-hour').flatpickr({
    //   enableTime: true,
    //   noCalendar: true,
    //   enableSeconds: false,
    //   time_24hr: true,
    //   dateFormat: 'H:i',
    //   defaultHour: 12,
    //   defaultMinute: 0,
    //   minDate: '06:00',
    //   maxDate: '23:00'
    // })


  })
}
