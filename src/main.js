import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/index'
import auth from './auth'
import vuepaypal from 'vue-paypal-checkout'
import ElementUI from 'element-ui'
import jquery from 'jquery'
import FBSignInButton from 'vue-facebook-signin-button'
import GoogleAuth from 'vue-google-auth'



global.jQuery = global.$ = jquery

// window.axios.defaults.headers.common = {
//   'X-Requested-With': 'XMLHttpRequest'
// }

require('bootstrap')

require('flatpickr')

require('owl.carousel')

Vue.use(ElementUI)
Vue.use(auth)
Vue.use(vuepaypal)
Vue.use(FBSignInButton)
Vue.config.productionTip = false

Vue.use(GoogleAuth, { client_id: '61723484159-thomjqt3dkm59mra2kma5f46g3copjva.apps.googleusercontent.com' })
Vue.googleAuth().load()

router.beforeEach((to, from, next) => {
  if (to.meta.requiredAuth) {
    const token = localStorage.getItem('token')
    var insta = to.query.token
    if(!token && insta){
      alert('Il suo account non è legato con il profilo social ')
    }
    if (!token) {
      next({name: 'Login'})
    }
  }
  if (to.meta.requiredServicesList) {
    const requiredServicesList = localStorage.getItem('requiredServicesList')
    if (requiredServicesList !== null) {
      next({name: 'ServicesList'})
    }
  }
  if (to.meta.requiredInsurances) {
    const requiredInsurances = localStorage.getItem('requiredInsurances')
    if (requiredInsurances !== null) {
      next({name: 'Insurances'})
    }
  }
  if (to.meta.requiredHome) {
    const requiredHome = localStorage.getItem('requiredHome')
    if (requiredHome === null) {
      next({name: 'Home'})
    }
  } else {
    if (to.meta.requiredSubscriptions) {
      const requiredSubscriptions = localStorage.getItem('requiredSubscriptions')
      if (requiredSubscriptions !== null) {
        next({name: 'Subscriptions'})
      }
    }
  }
  if (to.meta.requiredOrderTime) {
    if (!store.getters.getRealtyTypeId) {
      next({name: 'Home'})
    }
  }
  next()
})

/* eslint-disable no-new */
jquery(document).ready(function () {
  new Vue({
    el: '#app',
    router,
    render: h => h(App),
    store: store
  })
})
