import axios from 'axios'
import { host } from '../config'

export default {
  getFAQData () {
    return axios.get(host + '/api/faq')
  }
}
