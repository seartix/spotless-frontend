import Vue from 'vue'
import axios from 'axios'
import { host } from '../config'

export default {
  applyCouponCode (data) {
    return axios.post(host + '/api/apply-coupon', data, {
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  }
}
