import axios from 'axios'
import { host } from '../config'

export default {
  getAll () {
    return axios.get(host + '/api/provinces')
  }
}
