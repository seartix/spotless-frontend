import axios from 'axios'
import { host } from '../config'

export default {
  calculatePriceFromMeters (meters, realtyTypeId) {
    return axios.get(host + '/api/get-price-from-meters', {
      params: {
        meters,
        realty_type_id: realtyTypeId,
      }
    })
  },
  calculatePriceFromHours (hours, realtyTypeId, date, time) {
    return axios.get(host + '/api/get-price-from-hours', {
      params: {
        hours,
        realty_type_id: realtyTypeId,
        date: date,
        time: time
      }
    })
  }
}
