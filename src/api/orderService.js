import Vue from 'vue'
import axios from 'axios'
import { host } from '../config'

export default {
  getOrderViewModel () {
    return axios.get(host + '/api/get-order-view-model?XDEBUG_SESSION_START=PHPSTORM')
  },
  getCheckboxServices(clean_id){
    return axios.get(host + '/api/get-checkbox-sevices',{
      params:{
        clean_type:clean_id
      }
    })
  },
  postOrder (data) {
    return axios.post(host + '/api/orders', data, {
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },

  deleteSubscription(){
    return axios.get(`${host}/api/order/subscription-delete`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  getOrders () {
    return axios.get(host + '/api/orders', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  getExpirationSubscription(){
    return axios.get(host + '/api/orders/subscription', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  changeStatus (orderId, status) {
    axios.defaults.headers.common = {
      'X-Requested-With': 'XMLHttpRequest'
    }
    return axios.post(host + '/api/change-status', {
      'order_id': orderId,
      'status': status
    }, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  acceptOrder (orderId, cleanerId) {
    axios.defaults.headers.common = {
      'X-Requested-With': 'XMLHttpRequest'
    }
    return axios.post(host + '/api/accept-order', {
      'order_id': orderId,
      'cleaner_id': cleanerId
    }, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  getOrdersByStatus (cleanerId, status) {
    axios.defaults.headers.common = {
      'X-Requested-With': 'XMLHttpRequest',
      'Authorization': 'Bearer ' + Vue.auth.getToken()
    }
    return axios.get(host + '/api/orders-by-status', {
      params: {
        'cleaner_id': cleanerId,
        'status': status
      }
    })
  },
  getHistory () {
    return axios.get(host + '/api/orders-archived', {
      headers: {
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  dublicateOrder (orderId, data) {
    return axios.post(`${host}/api/copy-order/${orderId}?XDEBUG_SESSION_START=PHPSTORM`, data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  acceptDublicateOrder(orderId){
    return axios.get(host + '/api/copy-order-accept/' + orderId, {
      headers: {
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  deleteOrder (oderId) {
    return axios.delete(host + '/api/delete-order/' + oderId, {
      headers: {
        'Authorization': 'Bearer ' + Vue.auth.getToken(),
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  },
  getStepAmount (data) {
    return axios.post(host + '/api/get-step-amount', data, {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + Vue.auth.getToken()
    })
  },
  getInvoice (id) {
    return axios.get(`${host}/api/invoice/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  renewSubscription () {
    return axios.get(`${host}/api/order/subscription-renew`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  getPriceSubscription () {
    return axios.get(`${host}/api/order/subscription-price`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  }
}
