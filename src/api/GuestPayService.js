import axios from 'axios'
import { host } from '../config'
import Vue from 'vue'
export default {
  initPay (order) {
    return axios.post(host + '/api/gestpay?XDEBUG_SESSION_START=PHPSTORM', order, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  initEditPay (order) {
    return axios.post(`${host}/api/change-order/${order.id}/gestpay`, order, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  redirect (url) {
    return window.open(url, 'payment', 'width=800, height=500')
  },
  checkOrder (token, id) {
    return axios.get(`${host}/api/check-gestpay-order`, {
      params: {
        token,
        id
      },
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  copyOrder(orderId,coupon,price){
    return axios.get(`${host}/api/gestpay-copy-order/` + orderId,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + Vue.auth.getToken()
        },
        params:{
          couponCode:coupon,
          price_without_subscription:price
        }
      })
  }
}
