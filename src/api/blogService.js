import axios from 'axios'
import { host } from '../config'

export default {
  getAll () {
    return axios.get(host + '/api/blog')
  },
  getArticle(id) {
    return axios.get(host + '/api/article/' + id)
  },
  setComment(data){
    return axios.post(host + '/api/article/comment',data)
  }
}
