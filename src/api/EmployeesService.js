import axios from 'axios'
import { host } from '../config'

export default {
  getData () {
    return axios.get(host + '/api/all-cleaners')
  },
  getLanguages () {
    return axios.get(host + '/api/languages')
  },
  sendFilter (data) {
    return axios.get(host + '/api/all-cleaners', {
      params: data,
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  },
  create (data) {
    return axios.post(host + '/api/employer', data, {
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  }
}
