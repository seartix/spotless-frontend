import Vue from 'vue'
import axios from 'axios'
import { host, clientId, clientSecret } from '../config'

export default {
  login (email, password) {
    const data = {
      client_id: clientId,
      client_secret: clientSecret,
      grant_type: 'password',
      username: email,
      password: password
    }
    return axios.post(host + '/oauth/token', data)
  },
  miscalculation(data){
    return axios.post(host + '/api/miscalculation', data)
  },
  getCurrentUser () {
    return axios.get(host + '/api/user?XDEBUG_SESSION_START=PHPSTORM', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  getAddressByUser(city_id,province_id){
    return axios.get(host + '/api/user/address', {
      params: {
       city:city_id,
       province:province_id
      }
    })
  },
  forgotPassword (data) {
    return axios.post(host + '/api/user/forgot', {
      email: data
    }, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
  },

  register (data) {
    return axios.post(host + '/api/users', data, {
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  },
  editUser (data) {
    return axios.post(host + '/api/...', data, {
      headers: {
        'Authorization': 'Bearer ' + Vue.auth.getToken(),
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  },

  getCleaners (data) {
    return axios.get(host + '/api/choose-cleaners', {
      params: data,
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  },
  createReview (cleanerId, text, rating, recommend) {
    return axios.post(host + '/api/review', {
      'cleaner_id': cleanerId,
      'text': text,
      'rating': rating,
      'recommend': recommend
    }, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken(),
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  },
  getAllCleaners () {
    return axios.get(`${host}/api/cleaners/`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  getUserCleaners () {
    return axios.get(`${host}/api/user-cleaners`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  getReviews (cleanerId) {
    return axios.get(`${host}/api/reviews`, {
      params: {
        cleaner_id: cleanerId
      },
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },

  setPassword (data) {
    return axios.post(host + '/api/user/setpassword', data, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
  },

  getAvailableDate(){
    return axios.get(`${host}/api/available-dates-for-cleaner`)
  },
  getAvailableTime(date,duration){
    return axios.get(`${host}/api/available-times-for-cleaner`,{
      params:{
        date:date,
        duration:duration,
      }
    })
  },
  subscribeUser(id){
    return axios.get(`${host}/api/subscribe-user`,{
      params:{
        user_id:id
      }
    })
  },
  subscribeUserByEmail(data){
    return axios.post(`${host}/api/subscribe-user-email`,{email:data})
  },
  setFacebookId(id){
      return axios.get(`${host}/api/set-facebook-id`,{
      params:{
        facebook_id:id
      },
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  getUserByFacebookId(id){
    return axios.get(`${host}/api/get-facebook-id`,{
      params:{
        facebook_id:id
      },
    })
  },
  setInstagramId(id){
    return axios.get(`${host}/api/set-instagram-id`,{
      params:{
        instagram_id:id
      },
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  setGoogleId(id){
    return axios.get(`${host}/api/set-google-id`,{
      params:{
        google_id:id
      },
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Vue.auth.getToken()
      }
    })
  },
  getGoogleId(id){
    return axios.get(`${host}/api/get-google-id`,{
      params:{
        google_id:id
      },
    })
  },
  sendFeedback(data){
    return axios.post(`${host}/api/send-feedback`,data)
  }
}
