import axios from 'axios'
import { host } from '../config'

export default {
  upload (data) {
    let newData = new FormData()
    newData.append('file', data)
    return axios.post(host + '/api/image', newData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Accept': '*'
      }
    })
  }
}
