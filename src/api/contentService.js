import axios from 'axios'
import { host } from '../config'

export default {
  getContent () {
    return axios.get(host + '/api/content')
  }
}
