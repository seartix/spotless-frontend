import axios from 'axios'
import { host } from '../config'

export default {
  reservation (data) {
    return axios.post(host + '/api/reservation', data, {
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  }
}
