import axios from 'axios'
import { host } from '../config'

export default {
  getRealtyTypes () {
    return axios.get(host + '/api/realty-types')
  },
  getAllRealtyTypesPriceHoursMeters () {
    return axios.get(host + '/api/realty-types-price-hours-meters')
  }
}
