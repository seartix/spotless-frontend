import axios from 'axios'
import { host } from '../config'

export default {
  getHoursMeters () {
    return axios.get(host + '/api/subservice-hours-meters')
  }
}
