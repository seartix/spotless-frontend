import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Subscriptions from '@/components/Subscriptions'
import Order from '@/components/Steps/OrderStep'
import ServicesList from '@/components/Steps/ServicesListStep'
import Insurances from '@/components/Steps/InsurancesStep'
import Registration from '@/components/Steps/RegistrationStep'
import Payment from '@/components/Steps/PaymentStep'
import PersonalArea from '@/components/PersonalArea'
import Login from '@/components/auth/Login'
import Register from '@/components/auth/Register'
import AboutUs from '@/components/AboutUs'
import ContactUs from '@/components/Contact'
import ResetPassword from '@/components/ResetPassword'
import Confidentiality from '@/components/Confidentiality'
import FAQ from '@/components/FAQ'
import Privacy from '@/components/Privacy'
import Conditions from '@/components/Conditions'
import Employees from '@/components/Employees'
import Tariffs from '@/components/Tariffs'
import NewEmployee from '@/components/NewEmployee'
import Reservation from '@/components/Reservation'
import AddPay from '@/components/AddPay'
import GestPayResponsePage from '@/components/GestPayResponsePage'
import Blog from '@/components/Blog'
import Article from '@/components/Article'
import Services from '../components/Services'
import Miscalculation from '../components/Miscalculation'
import MainServiceList from '../components/Steps/MainServiceList'
import Abonement from '../components/Steps/AbonementStep'
import DateStep from '../components/Steps/DateStep'
import DateStepSubscription from '../components/Steps/DateStepSubscription'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/blog',
      name: 'Blog',
      component: Blog
    },
    {
      path: '/blog/:id',
      name: 'Article',
      component: Article
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about-us',
      name: 'AboutUs',
      component: AboutUs
    },
    {
      path: '/addpay',
      name: 'AddPay',
      component: AddPay
    },
    {
      path: '/gestpay-response/:status/',
      name: 'GestPayResponsePage',
      component: GestPayResponsePage
    },
    {
      path: '/reservation',
      name: 'Reservation',
      component: Reservation
    },
    {
      path: '/tariffs',
      name: 'tariffs',
      component: Tariffs
    },
    {
      path: '/NewEmployee',
      name: 'newEmployee',
      component: NewEmployee
    },
    {
      path: '/confidentiality',
      name: 'Confidentiality',
      component: Confidentiality
    },
    {
      path: '/contact-us',
      name: 'ContactUs',
      component: ContactUs
    },
    {
      path: '/subscriptions',
      name: 'Subscriptions',
      component: Subscriptions
    },
    {
      path: '/privacy',
      name: 'privacy',
      component: Privacy
    },
    {
      path: '/conditions',
      name: 'conditions',
      component: Conditions
    },
    {
      path: '/employees',
      name: 'employees',
      component: Employees
    },
    {
      path: '/personal-area',
      name: 'PersonalArea',
      component: PersonalArea,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/faq',
      name: 'faq',
      component: FAQ
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    // {
    //   path: '/services',
    //   name: 'Services',
    //   component: Services
    // },
    {
      path: '/miscalculation',
      name: 'Miscalculation',
      component: Miscalculation
    },
    {
      path: '/service',
      component: Services,
      name: 'Services',
      children: [
        {
          path: '/main-services-list',
          component: MainServiceList,
          name: 'MainServiceList',
          // meta: {
          //   requiredOrderTime: true
          // }
        },
        {
          path: '/services-list',
          component: ServicesList,
          name: 'ServicesList',
          meta: {
            requiredOrderTime: true
          }
        },
        {
          path: '/abonement',
          component: Abonement,
          name: 'Abonement',
          // meta: {
          //   requiredOrderTime: true
          // }
        },
        {
          path: '/choose-date',
          component: DateStep,
          name: 'Date',
          // meta: {
          //   requiredOrderTime: true
          // }
        },
        {
          path: '/choose-date-subscription',
          component: DateStepSubscription,
          name: 'DateSubscription',
          // meta: {
          //   requiredOrderTime: true
          // }
        },
        // {
        //   path: '/insurances',
        //   component: Insurances,
        //   name: 'Insurances',
        //   // meta: {
        //   //   requiredOrderTime: true
        //   // }
        // },
        {
          path: '/registration',
          component: Registration,
          name: 'Registration',
          meta: {
            requiredOrderTime: true
          }
        },
        {
          path: '/payment',
          component: Payment,
          name: 'Payment',
          meta: {
            requiredOrderTime: true,
            requiredAuth: true
          }
        }
      ]
    },
    {
      path: '/resetpassword/:token',
      component: ResetPassword
    }
  ]
})
