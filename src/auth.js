export default function (Vue) {
  Vue.auth = {
    setToken (token, expiration) {
      localStorage.setItem('token', token)
      localStorage.setItem('expiration', expiration)
    },

    getToken () {
      const token = localStorage.getItem('token')
      const expiration = localStorage.getItem('expiration')

      if (!token || !expiration) {
        return null
      } else {
        return token
      }
    },

    deleteToken () {
      localStorage.removeItem('token')
      localStorage.removeItem('expiration')
    },

    isAuthenticated () {
      return !!this.getToken()
    }
  }

  Object.defineProperties(Vue.prototype, {
    $auth: {
      get: () => Vue.auth
    }
  })
}
